package com.lxbluem.irc.pack;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PackInfoValidator {
    public List<String> validate(PackInfo packInfo) {
        List<String> errors = new ArrayList<>();

        if (StringUtils.isEmpty(packInfo.getServerHostName())) {
            errors.add("ServerHostName not provided");
        }

        if (StringUtils.isEmpty(packInfo.getChannelName())) {
            errors.add("ChannelName not provided");
        }

        if (StringUtils.isEmpty(packInfo.getNickName())) {
            errors.add("NickName not provided");
        }

        if (StringUtils.isEmpty(packInfo.getPackName())) {
            errors.add("PackName not provided");
        }

        if (packInfo.getPackNumber() == 0) {
            errors.add("PackNumber not provided");
        }

        return errors;
    }
}
