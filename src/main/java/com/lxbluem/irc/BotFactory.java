package com.lxbluem.irc;

import com.lxbluem.irc.pack.PackInfo;
import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BotFactory {

    public BotFactory() {
    }

    public PircBotX getBotForPackInfo(PackInfo packInfo) {
        Configuration.Builder configurationBuilder = new Configuration.Builder();
        configurationBuilder.addServer(packInfo.getServerHostName(), packInfo.getServerPort());
        configurationBuilder.setName(createBotName("P_"));
        configurationBuilder.addAutoJoinChannel(packInfo.getChannelName());

        configurationBuilder.setDccPorts(
                IntStream
                        .rangeClosed(3400, 3420)
                        .boxed()
                        .collect(Collectors.toList())
        );

        Configuration configuration = configurationBuilder.buildConfiguration();

        return new PircBotX(configuration);
    }

    private String createBotName(String prefix) {
        StringBuilder s = new StringBuilder(prefix);
        String allowedChars = "abcdefghijklmnopqrstuvwxyz1234567890";
        for (int i = 0; i < 2; i++) {
            int index = (int) (Math.random() * allowedChars.length());
            char charAt = allowedChars.charAt(index);
            s.append(charAt);
        }

        return s.toString();
    }


}
