package com.lxbluem.irc;

public enum DccRequestState {
    NEW, REQUEST, TRANSFER, DONE, ERROR, INCOMING, QUEUE, TRANSFER_ERROR
}
