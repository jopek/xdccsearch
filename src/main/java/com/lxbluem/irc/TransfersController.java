package com.lxbluem.irc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lxbluem.ControllerBase;
import com.lxbluem.ErrorResponse;
import com.lxbluem.irc.pack.PackInfo;
import com.lxbluem.irc.pack.PackValidationException;

import javax.inject.Inject;
import java.net.HttpURLConnection;

import static spark.Spark.delete;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.post;

public class TransfersController extends ControllerBase {
    private final DccService dccService;
    private final ObjectMapper objectMapper;

    @Inject
    public TransfersController(DccService dccService, ObjectMapper objectMapper) {
        super("/xfers");
        this.dccService = dccService;
        this.objectMapper = objectMapper;
    }

    public void setup() {
        get(getRootRoute(), (request, response) -> {
            return dccService.getTransfers();
        }, objectMapper::writeValueAsString);

        post(getRootRoute(), (request, response) -> {
            PackInfo packInfo = objectMapper.readValue(request.bodyAsBytes(), PackInfo.class);
            int xferNum = dccService.startTransfer(packInfo);

            response.header("xferNum", String.valueOf(xferNum));
            response.status(HttpURLConnection.HTTP_CREATED);

            return dccService.getTransfers();
        }, objectMapper::writeValueAsString);

        delete(getRootRoute("/:packId"), (request, response) -> {
            String id = request.params("packId");
            int packId = Integer.parseInt(id);
            dccService.removeTransfer(packId);
            response.status(HttpURLConnection.HTTP_OK);
            return dccService.getTransfers();
        });

        exception(NumberFormatException.class, (exception, req, res) -> {
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.message = exception.getClass().getName();

            String serialized = null;
            try {
                serialized = objectMapper.writeValueAsString(errorResponse);
            } catch (JsonProcessingException ignored) {
            }

            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            res.body(serialized);
        });

        exception(PackValidationException.class, (exception, req, res) -> {
            StringBuilder sb = new StringBuilder("Validation Errors: ");
            ((PackValidationException) exception).getValidationErrors().stream().forEach(sb::append);

            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.message = sb.toString();

            String serialized = null;
            try {
                serialized = objectMapper.writeValueAsString(errorResponse);
            } catch (JsonProcessingException ignored) {
            }

            res.status(HttpURLConnection.HTTP_BAD_REQUEST);
            res.body(serialized);
        });
    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public void transferControl(
//        @PathVariable("id") int transferNum,
//        @RequestBody TransferControlDto transferControlDto
//    )
//    {
//        switch (transferControlDto.command) {
//            case "stop":
//                dccService.stopTransfer(transferNum);
//                break;
//            case "resume":
//                dccService.resumeTransfer(transferNum);
//                break;
//        }
//    }
}
