package com.lxbluem.irc;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdditionalChannelsProvider {
    private Map<String, Map<String, Set<String>>> networkChannelMap;

    public AdditionalChannelsProvider() {
        networkChannelMap = new HashMap<>();
    }

    public Set<String> getAdditionalChannels(String networkName, String channelName) {
        validateSelectors(networkName, channelName);

        if (!networkChannelMap.containsKey(networkName))
            return Collections.emptySet();

        if (!networkChannelMap.get(networkName).containsKey(channelName))
            return Collections.emptySet();

        return networkChannelMap.get(networkName).get(channelName);
    }

    public void addAdditionalChannel(String networkName, String channelName, String additionalChannelName) {
        validateSelectors(networkName, channelName);
        validateAdditionalChannelName(additionalChannelName);

        if (!networkChannelMap.containsKey(networkName)) {
            Set<String> channelSet = new HashSet<>();
            channelSet.add(additionalChannelName);

            Map<String, Set<String>> channelMap = new HashMap<>();
            channelMap.put(channelName, channelSet);

            networkChannelMap.put(networkName, channelMap);
            return;
        }

        if (!networkChannelMap.get(networkName).containsKey(channelName)) {
            Set<String> channelSet = new HashSet<>();
            channelSet.add(additionalChannelName);

            networkChannelMap.get(networkName).put(channelName, channelSet);
            return;
        }

        networkChannelMap.get(networkName).get(channelName).add(additionalChannelName);
    }

    private void validateAdditionalChannelName(String additionalChannelName) {
        if (StringUtils.isEmpty(additionalChannelName)) {
            throw new IllegalArgumentException("additional channel name may not be null nor empty");
        }
    }

    private void validateSelectors(String networkName, String channelName) {
        if (StringUtils.isEmpty(networkName)) {
            throw new IllegalArgumentException("network name may not be null nor empty");
        }

        if (StringUtils.isEmpty(channelName)) {
            throw new IllegalArgumentException("channel name may not be null nor empty");
        }
    }
}
