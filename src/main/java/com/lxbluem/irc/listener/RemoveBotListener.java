package com.lxbluem.irc.listener;

import com.lxbluem.irc.DccService;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoveBotListener extends ListenerAdapter {
    private static Logger logger = LoggerFactory.getLogger(DccService.class);

    @Override
    public void onMessage(MessageEvent event) throws Exception {
        if (event.getMessage().startsWith("?removeListener")) {
            event.getBot().sendIRC().quitServer("goodbye cruel world!");
            logger.info("{} exiting", event.getBot().getUserBot().getNick());
        }
    }
}
