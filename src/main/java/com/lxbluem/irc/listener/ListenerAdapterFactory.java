package com.lxbluem.irc.listener;

import com.lxbluem.irc.AdditionalChannelsProvider;
import com.lxbluem.irc.pack.PackInfo;

public class ListenerAdapterFactory {
    private final AdditionalChannelsProvider channelsProvider;
    private final TransferFileHandler fileHandler;

    public ListenerAdapterFactory(AdditionalChannelsProvider channelsProvider, TransferFileHandler transferFileHandler) {
        this.channelsProvider = channelsProvider;
        fileHandler = transferFileHandler;
    }

    public TransferListener getTransferListener(PackInfo packInfo, String botName) {
        return new TransferListener(packInfo, botName, channelsProvider, fileHandler);
    }
}
