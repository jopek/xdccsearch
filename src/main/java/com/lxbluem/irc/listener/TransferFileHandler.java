package com.lxbluem.irc.listener;

import com.lxbluem.configuration.XdccConfiguration;

import java.io.File;
import java.nio.file.FileAlreadyExistsException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TransferFileHandler {
    private final XdccConfiguration xdccConfiguration;
    DownloadsDirectory downloadsDirectory;
    private String downloadDir;
    private TransferFileNameMapper nameMapper;

    public TransferFileHandler(XdccConfiguration xdccConfiguration, TransferFileNameMapper nameMapper) {
        this.xdccConfiguration = xdccConfiguration;
        this.nameMapper = nameMapper;
        downloadsDirectory = new DownloadsDirectory();
    }

    public File saveTo(String packFilename, long packid) throws FileAlreadyExistsException {
        setDownloadsDirectory();

        String partName = nameMapper.fsPartName(packFilename, packid);

        List<File> fileList = downloadsDirectory.getFileList(downloadDir);
        Optional<File> fileOptional = fileList
                .stream()
                .filter(f -> f.getName().equals(partName))
                .findFirst();

        if (fileOptional.isPresent()) {
            throw new FileAlreadyExistsException(partName);
        }

        String partialDownloadFilename = String.format("%s/%s", downloadDir, partName);

        return new File(partialDownloadFilename);
    }


    public void finishedDownloadingFile(File file) {
        String filename = file.getPath().replace(".part", "");
        file.renameTo(new File(filename));
    }

    private void setDownloadsDirectory() {
        try {
            downloadDir = xdccConfiguration.getApplicationSettings("downloadDir", String.class);
        } catch (NoSuchFieldException e) {
            downloadDir = "downloads";
        }
    }

    static class DownloadsDirectory {
        List<File> getFileList(String downloadDir) {
            File[] files = new File(downloadDir).listFiles();
            if (files == null)
                return Collections.emptyList();
            return Arrays.asList(files);
        }
    }

}
