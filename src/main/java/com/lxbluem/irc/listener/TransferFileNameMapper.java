package com.lxbluem.irc.listener;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableLong;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toList;

public class TransferFileNameMapper {

    public String packName(String name) {
        List<String> split = getNameElements(name);
        final String PART = "part";
        final String PATTERN = "_\\d+_";

        if (split.size() == 1)
            return name;

        Pattern p = Pattern.compile(PATTERN);

        MutableLong patternCount = new MutableLong(split.stream().filter(s -> p.matcher(s).find()).count());
        MutableLong partCount = new MutableLong(split.stream().filter(s -> s.equalsIgnoreCase(PART)).count());

        List<String> nameElements = split.stream()
                .filter(s -> noPatternMatch(s, p, patternCount))
                .filter(s -> noStringMatch(s, PART, partCount))
                .collect(toList());

        return StringUtils.join(nameElements, '.');
    }

    private List<String> getNameElements(String name) {
        return new ArrayList<>(Arrays.asList(name.split("\\.")));
    }

    private boolean noPatternMatch(String s, Pattern p, MutableLong mutableLong) {
        if (p.matcher(s).find()) {
            mutableLong.decrement();
            return mutableLong.getValue() > 0;
        }
        return true;
    }

    private boolean noStringMatch(String s, String p, MutableLong mutableLong) {
        if (p.equalsIgnoreCase(s)) {
            mutableLong.decrement();
            return mutableLong.getValue() > 0;
        }
        return true;
    }

    public String fsPartName(String filename, long packid) {
        List<String> nameElements = getNameElements(filename);
        int size = nameElements.size();

        if (size == 1) {
            nameElements.add(String.format("_%d_", packid));
        } else {
            String extension = nameElements.get(size - 1);
            nameElements.remove(size - 1);
            nameElements.add(String.format("_%d_", packid));
            nameElements.add(extension);
        }
        nameElements.add("part");
        return StringUtils.join(nameElements, '.');
    }

    public int createHash(String safeFilename, long filesize) {
        String toBeHashed = safeFilename + filesize;
        int hashCode = toBeHashed.hashCode();
        return Math.abs(hashCode);
    }
}
