package com.lxbluem;

public abstract class ControllerBase {
    public static final String API = "/api";
    private String urlPrefix;

    public ControllerBase(String urlPrefix) {
        this.urlPrefix = API + urlPrefix;
    }

    public String getRootRoute() {
        return urlPrefix;
    }

    public String getRootRoute(String route) {
        return urlPrefix + route;
    }
}
