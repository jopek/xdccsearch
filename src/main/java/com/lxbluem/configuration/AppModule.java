package com.lxbluem.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lxbluem.irc.AdditionalChannelsProvider;
import com.lxbluem.irc.BotFactory;
import com.lxbluem.irc.DccService;
import com.lxbluem.irc.TransfersController;
import com.lxbluem.irc.listener.ListenerAdapterFactory;
import com.lxbluem.irc.listener.TransferFileHandler;
import com.lxbluem.irc.listener.TransferFileNameMapper;
import com.lxbluem.irc.pack.PackInfoValidator;
import com.lxbluem.search.ClientSettings;
import com.lxbluem.search.SearchClient;
import com.lxbluem.search.SearchController;
import com.lxbluem.search.SearchService;
import dagger.Lazy;
import dagger.Module;
import dagger.Provides;
import org.pircbotx.Configuration;
import org.pircbotx.MultiBotManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;

@Module
public class AppModule {
    private static final Logger log = LoggerFactory.getLogger(AppModule.class);

    @Provides
    public SearchController provideSearchController(SearchService searchService, ObjectMapper objectMapper) {
        log.debug("AppModule.provideSearchController");
        return new SearchController(searchService, objectMapper);
    }

    @Provides
    public TransfersController provideTransfersController(DccService dccService, ObjectMapper objectMapper) {
        log.debug("AppModule.provideSearchController");
        return new TransfersController(dccService, objectMapper);
    }

    @Provides
    @Singleton
    public DccService provideDccService(
            BotFactory botFactoryProvider,
            ListenerAdapterFactory listenerAdapterFactory,
            MultiBotManager manager,
            PackInfoValidator packInfoValidator
    ) {
        log.debug("AppModule.provideDccService");
        return new DccService(botFactoryProvider, listenerAdapterFactory, manager, packInfoValidator);
    }

    @Provides
    public BotFactory providePircBotXFactory() {
        log.debug("AppModule.providePircBotXFactory");
        return new BotFactory();
    }

    @Provides
    public Configuration.Builder provideConfigurationBuilder() {
        return new Configuration.Builder();
    }

    @Provides
    @Singleton
    public ListenerAdapterFactory provideListenerAdapterFactory(AdditionalChannelsProvider channelsProvider, TransferFileHandler fileHandler) {
        log.debug("AppModule.provideListenerAdapterFactory");
        return new ListenerAdapterFactory(channelsProvider, fileHandler);
    }

    @Provides
    @Singleton
    public MultiBotManager provideMultiBotManager() {
        log.debug("AppModule.provideMultiBotManager");
        MultiBotManager multiBotManager = new MultiBotManager();
        multiBotManager.start();
        return multiBotManager;
    }

    @Provides
    @Singleton
    public PackInfoValidator providePackInfoValidator() {
        log.debug("AppModule.providePackInfoValidator");
        return new PackInfoValidator();
    }

    @Provides
    public SearchService provideSearchService(Lazy<SearchClient> searchClient) {
        log.debug("AppModule.provideSearchService");
        return new SearchService(searchClient);
    }

    @Provides
    public ObjectMapper provideObjectMapper() {
        log.debug("AppModule.provideObjectMapper");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @Provides
    @Singleton
    public AdditionalChannelsProvider provideAdditionalChannelsProvider() {
        log.debug("AppModule.provideAdditionalChannelsProvider");
        return new AdditionalChannelsProvider();
    }

    @Provides
    @Singleton
    public TransferFileHandler provideTransferFileHandler(XdccConfiguration xdccConfiguration, TransferFileNameMapper nameMapper) {
        log.debug("AppModule.provideTransferFileHandler");
        return new TransferFileHandler(xdccConfiguration, nameMapper);
    }

    @Provides
    @Singleton
    public XdccConfiguration provideXdccConfiguration() {
        log.debug("AppModule.provideXdccConfiguration");
        return new XdccConfiguration();
    }

    @Provides
    public TransferFileNameMapper provideTransferFileNameMapper() {
        log.debug("AppModule.provideTransferFileNameMapper");
        return new TransferFileNameMapper();
    }

    @Provides
    public SearchClient provideSearchClient(ObjectMapper objectMapper, XdccConfiguration configuration) {
        log.debug("AppModule.provideSearchClient");
        ClientSettings settings = null;
        try {
            settings = configuration.getApplicationSettings("clients", ClientSettings.class);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            settings = new ClientSettings();
            settings.setHost("localhost");
            settings.setPort(4567);
            settings.setPath("/api");
            settings.setQueryParam("q");
            settings.setPageParam("pn");
        }
        return new SearchClient(settings, objectMapper);
    }

}
