package com.lxbluem;

import spark.Spark;

public class WebConfiguration {
    public static void setupStaticResourceLocation() {
        Spark.staticFileLocation("/public");
    }
}
