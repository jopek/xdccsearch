package com.lxbluem.irc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lxbluem.irc.listener.ListenerAdapterFactory;
import com.lxbluem.irc.listener.TransferListener;
import com.lxbluem.irc.pack.PackInfo;
import com.lxbluem.irc.pack.PackInfoValidator;
import com.lxbluem.irc.pack.PackValidationException;
import org.junit.Before;
import org.junit.Test;
import org.pircbotx.Configuration;
import org.pircbotx.MultiBotManager;
import org.pircbotx.PircBotX;
import org.pircbotx.hooks.managers.ListenerManager;
import org.pircbotx.output.OutputIRC;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class DccServiceTest {

    public static final String SELFNAME = "botA";
    public static final String PACK_BOT_NAME = "mybotdcc";
    public static final int PACK_ID = 12345;
    private PircBotX bot = mock(PircBotX.class);
    private OutputIRC outputIRC = mock(OutputIRC.class);
    private BotFactory botFactory = mock(BotFactory.class);
    private TransferListener transferListener = mock(TransferListener.class);
    private ListenerManager listenerManager = mock(ListenerManager.class);
    private ListenerAdapterFactory listenerAdapterFactory = mock(ListenerAdapterFactory.class);
    private MultiBotManager botManager = mock(MultiBotManager.class);
    private PackInfoValidator packInfoValidator = mock(PackInfoValidator.class);
    private Configuration configuration = mock(Configuration.class);

    private PackInfo packInfo;
    private Map<String, Object> packInfoSampleData;


    private DccService dccService;

    @Before
    public void setUp() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        packInfoSampleData = new HashMap<>();
        packInfoSampleData.put("name", "1mb testfile");
        packInfoSampleData.put("nname", "super duper alpha irc");
        packInfoSampleData.put("naddr", "192.168.99.100");
        packInfoSampleData.put("nport", 6667);
        packInfoSampleData.put("cname", "#DOWNLOAD");
        packInfoSampleData.put("uname", PACK_BOT_NAME);
        packInfoSampleData.put("n", 3);
        packInfoSampleData.put("pid", PACK_ID);
        packInfo = mapper.convertValue(packInfoSampleData, PackInfo.class);

        when(botFactory.getBotForPackInfo(any(PackInfo.class))).thenReturn(bot);
        when(listenerAdapterFactory.getTransferListener(any(PackInfo.class), anyString()))
                .thenReturn(transferListener);
        when(botManager.getBotById(anyInt())).thenReturn(bot);
        when(packInfoValidator.validate(any(PackInfo.class))).thenReturn(Collections.<String>emptyList());
        when(configuration.getName()).thenReturn(SELFNAME);
        when(configuration.getListenerManager()).thenReturn(listenerManager);
        when(bot.getConfiguration()).thenReturn(configuration);
        when(bot.send()).thenReturn(outputIRC);
        when(bot.sendIRC()).thenReturn(outputIRC);

        dccService = new DccService(botFactory, listenerAdapterFactory, botManager, packInfoValidator);
    }

    @Test
    public void startTransfer_creates_new_bot() throws PackValidationException {
        dccService.startTransfer(packInfo);

        verify(packInfoValidator).validate(packInfo);
        verify(botFactory).getBotForPackInfo(packInfo);
        verify(bot).getConfiguration();
        verify(listenerAdapterFactory).getTransferListener(same(packInfo), same(SELFNAME));
        verify(configuration).getListenerManager();
        verify(listenerManager).addListener(same(transferListener));
        verify(botManager).addBot(bot);

        assertEquals(1, dccService.transferListenerMap.size());
    }

    @Test (expected = PackValidationException.class)
    public void startTransfer_throws_exception_on_invalid_packInfo() throws PackValidationException {
        when(packInfoValidator.validate(packInfo)).thenReturn(Arrays.asList("error1", "error2"));

        dccService.startTransfer(packInfo);
    }

    @Test
    public void removeTransfer_removes_transfer() {
        dccService.transferListenerMap.put(1, transferListener);
        when(transferListener.getPack()).thenReturn(packInfo);

        dccService.removeTransfer(PACK_ID);

        verifyZeroInteractions(outputIRC);
        assertEquals(0, dccService.transferListenerMap.size());
    }

    @Test
    public void removeTransfer_removes_transfer_even_when_no_bot_available() {
        dccService.transferListenerMap.put(1, transferListener);
        when(transferListener.getPack()).thenReturn(packInfo);
        when(botManager.getBotById(anyInt())).thenReturn(null);

        dccService.removeTransfer(PACK_ID);

        verifyZeroInteractions(outputIRC);
        assertEquals(0, dccService.transferListenerMap.size());
    }

    @Test
    public void removeTransfer_removes_transfer_and_quits_network() {
        dccService.transferListenerMap.put(1, transferListener);
        when(transferListener.getPack()).thenReturn(packInfo);
        when(bot.isConnected()).thenReturn(true);

        dccService.removeTransfer(PACK_ID);

        verify(outputIRC).message(PACK_BOT_NAME, "XDCC REMOVE");
        verify(outputIRC).quitServer();
        assertEquals(0, dccService.transferListenerMap.size());
    }

    @Test
    public void removeTransfer_does_nothing_if_no_transfer_found() {
        dccService.removeTransfer(PACK_ID);

        verifyZeroInteractions(botManager);
        verifyZeroInteractions(outputIRC);
        assertEquals(0, dccService.transferListenerMap.size());
    }

    @Test
    public void getTransfers_returns_list_of_transfers() {
        when(transferListener.getStatus()).thenReturn(new TransferStatusResponse());

        dccService.transferListenerMap.put(1, transferListener);
        dccService.transferListenerMap.put(3, transferListener);
        dccService.transferListenerMap.put(13, transferListener);
        dccService.transferListenerMap.put(7, transferListener);

        List<TransferStatusResponse> transfers = dccService.getTransfers();

        verify(transferListener, times(4)).getStatus();
        assertEquals(4, transfers.size());
    }

    @Test
    public void getTransfers_returns_list_of_transfers_excluding_null_listeners() {
        when(transferListener.getStatus()).thenReturn(new TransferStatusResponse());

        dccService.transferListenerMap.put(1, transferListener);
        dccService.transferListenerMap.put(3, transferListener);
        dccService.transferListenerMap.put(13, transferListener);
        dccService.transferListenerMap.put(7, null);

        List<TransferStatusResponse> transfers = dccService.getTransfers();

        verify(transferListener, times(3)).getStatus();
        assertEquals(3, transfers.size());
    }

}