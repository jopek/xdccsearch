package com.lxbluem.irc.listener;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TransferFileNameMapperTest {

    TransferFileNameMapper mapper = new TransferFileNameMapper();

    @Test
    public void name_to_packname() {
        assertEquals("filename", mapper.packName("filename"));
        assertEquals("filename", mapper.packName("filename._123_"));
        assertEquals("filename", mapper.packName("filename._123_.part"));
        assertEquals("filename.txt", mapper.packName("filename.txt"));
        assertEquals("file.name.mkv", mapper.packName("file.name.mkv"));
        assertEquals("file.name.mkv", mapper.packName("file.name._123_.mkv"));
        assertEquals("file.name.mkv", mapper.packName("file.name._123_.mkv.part"));
        assertEquals("file.name._123_.mkv", mapper.packName("file.name._123_._123_.mkv"));
        assertEquals("file.name._123_.part.mkv", mapper.packName("file.name._123_.part._123_.mkv.part"));
    }

    @Test
    public void packname_to_name() {
        assertEquals("filename._123123_.part", mapper.fsPartName("filename", 123123L));
        assertEquals("filename._200_.txt.part", mapper.fsPartName("filename.txt", 200L));
    }
}