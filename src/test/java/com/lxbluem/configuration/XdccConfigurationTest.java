package com.lxbluem.configuration;

import com.google.common.collect.Lists;
import com.lxbluem.irc.listener.TransferFileDescription;
import com.lxbluem.search.ClientSettings;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.NoSuchElementException;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class XdccConfigurationTest {

    private XdccConfiguration configuration;
    private List<TransferFileDescription> savedTransfers;

    @Before
    public void setup() throws IOException {
        configuration = new XdccConfiguration();
        configuration.transfersFileWriter = new StringWriter();

        ClassLoader loader = getClass().getClassLoader();
        InputStream inputStream = loader.getResourceAsStream(configuration.TRANSFERS_YML);
        configuration.transfersFileReader = new InputStreamReader(inputStream);

        savedTransfers = Lists.newArrayList(
                new TransferFileDescription("asd.234", "asd", 345345345, 123123L),
                new TransferFileDescription("wer.674", "wer", 120445, 7567L),
                new TransferFileDescription("zxc.567", "zxc", 900000, 34233L)
        );
    }

    @Test
    public void getApplicationSetting_load_should_load_config() throws Exception {
        ClientSettings clients = configuration.getApplicationSettings("clients", ClientSettings.class);
        assertNotNull(clients);
    }

    @Test (expected = NoSuchElementException.class)
    public void getApplicationSetting_load_fails_to_load_unknown_config() throws Exception {
        configuration.getApplicationSettings("unknown", ClientSettings.class);
    }

    @Test
    public void getSavedTransfers_should_load() throws Exception {
        List<TransferFileDescription> loadedSavedTransfers = configuration.getSavedTransfers();

        assertEquals(savedTransfers.size(), loadedSavedTransfers.size());
        for (TransferFileDescription desc : loadedSavedTransfers) {
            assertTrue("cannot find " + desc + " in resource loaded transfers.", savedTransfers.contains(desc));
        }
    }

    @Test
    public void getTransfers_getSavedTransfers_should_load() throws Exception {
        configuration.transfersFileWriter = mock(Writer.class);
        configuration.saveTransfers(savedTransfers);
        verify(configuration.transfersFileWriter, atLeastOnce()).flush();

        configuration.transfersFileWriter = new StringWriter();
        configuration.saveTransfers(savedTransfers);
        String s = configuration.transfersFileWriter.toString();
    }

}
